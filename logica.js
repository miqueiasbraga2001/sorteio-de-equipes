$(window).scrollTop(0);


$('#gerar').click(function () {

    let q_equipes = $('#q_equipes').val()

    let texto = $('#texto').val()

    if (texto != "") {

        ///////////////////Remove quebras de linha
        texto = texto.replace(/(\r\n|\n|\r)/gm, ",");

        let palavras = [];
        let cont = 0;

        //////////////////Separa as palavras do texto em um indice do array
        for (let i = 0; i < texto.length; i++) {
            if (texto.charAt(i) != ',') {

                if (palavras[cont] == undefined) {
                    palavras[cont] = "";
                }

                palavras[cont] += texto.charAt(i);
            } else {
                cont++;
            }
        }

        //////////////////Remove Vazios
        palavras = palavras.filter(function (i) {
            return i;
        });

        // Função para randomizar array
        palavras = shuffleArray(palavras)

        ////////////limite maximo que cada equipe pode conter
        let limite = Math.ceil(palavras.length / q_equipes);

        criar_equipes(palavras, q_equipes, limite);

    }else{   
        $(window).scrollTop(0);
        setInterval(function () {$('body').css('overflow-y', 'hidden')}, 500);
    }
})

function criar_equipes(palavras, q_equipes, limite) {

    //Limpa Section
    $('#equipes').html('')
    $('body').css('overflow-y', 'auto')
    window.location.href='#equipes';

    let tamanho_col = 12 / q_equipes;

    let passado = 0;
    //Repete a quantidade de equipes
    for (let i = 0; i < q_equipes; i++) {
        $('#equipes').append("<div id='equipe-" + i + "' class='col-md-" + tamanho_col  + " col-sm-6 col-12'></div>");
        $('#equipe-' + i).append("<h2>Equipe " + (i + 1) + "</h2>")

        for (let a = 0; a < limite; a++) {

            if (palavras[a + passado] != undefined) {
                $('#equipe-' + i).append("<span>" + palavras[a + passado] + "</span>")
                $('#equipe-' + i).append("<br></br>")
            }

        }

        passado += limite;
    }

}

// Função para randomizar array
function shuffleArray(arr) {
    // Loop em todos os elementos
    for (let i = arr.length - 1; i > 0; i--) {
        // Escolhendo elemento aleatório
        const j = Math.floor(Math.random() * (i + 1));
        // Reposicionando elemento
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    // Retornando array com aleatoriedade
    return arr;
}